package com.example.gamer.mvvm.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.gamer.mvvm.repository.Repository;
import com.example.gamer.mvvm.room.model.Note;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private Repository repository;
    private LiveData<List<Note>> noteList;

    public NoteViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
        noteList = repository.getAllNotes();
    }

    public void update(Note note){
        repository.update(note);
    }
    public void insert(Note note){
        repository.insert(note);
    }
    public void delete(Note note){
        repository.delete(note);
    }
    public void deleteAllNote(){
        repository.deleteAllNote();
    }
    public LiveData<List<Note>> getAllNotes(){
        return noteList;
    }
}
