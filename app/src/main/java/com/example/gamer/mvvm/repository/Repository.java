package com.example.gamer.mvvm.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.gamer.mvvm.room.NoteDatabase;
import com.example.gamer.mvvm.room.dao.NoteDao;
import com.example.gamer.mvvm.room.model.Note;

import java.util.List;

public class Repository {
    private NoteDao noteDao;
    private LiveData<List<Note>> notes;

    public Repository(Application application){
        NoteDatabase noteDatabase = NoteDatabase.getInstance(application);
        noteDao = noteDatabase.noteDao();
        notes = noteDao.getAllNotes();
    }

    public void insert(Note note){
        new Insert(noteDao).execute(note);
    }
    public void update(Note note){
        new Update(noteDao).execute(note);
    }
    public void delete(Note note){
        new Delete(noteDao).execute(note);
    }
    public void deleteAllNote(){
        new DeleteAllNote(noteDao).execute();
    }
    public LiveData<List<Note>> getAllNotes(){
        return notes;
    }


    private static class Insert extends AsyncTask<Note,Void,Void> {
        private NoteDao noteDao;
        private Insert(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.insert(notes[0]);
            return null;
        }
    }

    private static class Update extends AsyncTask<Note,Void,Void> {
        private NoteDao noteDao;
        private Update(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.update(notes[0]);
            return null;
        }
    }

    private static class Delete extends AsyncTask<Note,Void,Void> {
        private NoteDao noteDao;
        private Delete(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.insert(notes[0]);
            return null;
        }
    }

    private static class DeleteAllNote extends AsyncTask<Void,Void,Void> {
        private NoteDao noteDao;
        private DeleteAllNote(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deleteAllNote();
            return null;
        }
    }
}
